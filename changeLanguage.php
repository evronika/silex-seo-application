<?php
/**
 * Created by PhpStorm.
 * User: veronika
 * Date: 8.7.18
 * Time: 13.21
 */
if (isset($_GET['lang'])){
    setcookie('lang', $_GET['lang'], time()+60*60*24);
    if (isset($_SERVER['HTTP_REFERER'])) {
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
}