<?php
include_once "./redirects.php";

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/vendor/twig/twig/views/',
));

$app->get('about', function () use ($app) {

});

$app->get('instruction', function () use ($app) {

    $startY = 2018;
    $nowY = date('Y');
    if ($startY == $nowY)
        $years = $startY;
    else
        $years = $startY . " - " . $nowY;
    if (($_COOKIE['lang']) == "ru")
        $content = array(
            'title' => 'Подключение файла',
            'description' => "Подключение файла с помощью .htaccess ",
            'content' => 'Мало кто знает, но в файле .htaccess есть возможность подключать файлы скриптов, которые 
            будут выполняться при каждом запросе. Вот и пример .htaccess',
            'code' => 'php_value auto_prepend_file "/dir/path/utilities.php" (в самом начале файла .htaccess)',
            'classActiveRu' => "class=active",
            'classActiveEn' => "",
            'language' => 'Изменить язык:',
            'years' => $years,
            'instruction' => 'Instruction',
            'use_slash' => 'Use / redirect',
            'use_www' => 'Use www redirect'
        );
    else
        $content = array(
            'title' => 'Connecting a file',
            'description' => "Connecting a file with using .htaccess ",
            'content' => 'A few people know, that there is an advantage to connect files of scripts in the file .htaccess.
             It will be executed with every request. For example .htaccess:',
            'code' => 'php_value auto_prepend_file "/dir/path/utilities.php" (в самом начале файла .htaccess)',
            'classActiveRu' => "class=active",
            'classActiveEn' => "",
            'language' => 'Изменить язык:',
            'years' => $years,
            'instruction' => 'Инструкция',
            'use_slash' => 'Использовать / редирект',
            'use_www' => 'Использовать www редирект'
        );
    return $app['twig']->render('template-instruction.html', $content);

});


$app->get('/', function (Request $request) use ($app) {

    $currentLanguage = 'ru';
    if (isset($_COOKIE['lang'])) {
        $currentLanguage = $_COOKIE['lang'];
    }

    $startY = 2018;
    $nowY = date('Y');
    if ($startY == $nowY)
        $years = $startY;
    else
        $years = $startY . " - " . $nowY;
    if ($currentLanguage == "ru")
        $content = array(
            'title' => 'Генерация редиректов',
            'description' => "Генерация редиректов в виде php файла, вставляемого в .htaccess ",
            'language' => 'Изменить язык:',
            'classActiveRu' => "class=active",
            'classActiveEn' => "",
            'h2' => 'Выберите нужные редиректы:',
            'minus_www' => 'Переадресация на без www.',
            'plus_www' => 'Переадресация на с www.',
            'plus_slash' => 'Переадресация на с /',
            'minus_slash' => 'Переадресация на без /',
            'csv_text' => 'CSV файл с 301 редиректами',
            'button_text' => 'Отправить',
            'index' => 'Переадресация на без index.php или index.html',
            'years' => $years,
            'instruction' => 'Инструкция',
            'use_slash' => 'Использовать / редирект',
            'use_www' => 'Использовать www редирект'

        );
    elseif ($currentLanguage == 'en')
        $content = array(
            'title' => 'REDIRECTS',
            'description' => "SELECT TYPES OF REDIRECTS",
            'language' => 'Change language:',
            'classActiveEn' => "class=active",
            'classActiveRu' => "",
            'h2' => 'Please select your prefered types of redirects:',
            'minus_www' => 'With www to without www',
            'plus_www' => 'Without www to With www',
            'plus_slash' => 'Without / to with /',
            'minus_slash' => 'With / to without /',
            'csv_text' => 'CSV file with 301 redirects',
            'button_text' => 'Submit',
            'index' => 'Use redirect with index.php, index.html to without it',
            'years' => $years,
            'instruction' => 'Instruction',
            'use_slash' => 'Use / redirect',
            'use_www' => 'Use www redirect'
        );
    return $app['twig']->render('template.html', $content);

});

$app->post('/', function (Request $request) use ($app) {
    $index = $request->get('index');
    $www = $request->get('www');
    $slash = $request->get('slash');
    $use_www = $request->get('use_www');
    $use_slash = $request->get('use_slash');
    $use_csv  = $request->get('use_csv');
    //work with csv
    $uploaddir = __DIR__ . '/uploads/';
    $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
    $filename = "redirects.php.txt";
    $file_path = $uploaddir . $filename;
    $old = $app['twig']->render('start.txt');
    $current = $old;
    if (($handle = fopen($uploadfile, "r")) !== FALSE && $uploadfile && (isset($use_csv))) {
    	$i = 1;
        while (($data_file = fgetcsv($handle, 1000, ",")) !== FALSE) {
        	if ($i==1) {
            	$current .= '$redirects[\'' . urldecode($data_file[0]) . '\'] = \'';
            	$i++;
        	}
        	else {
        		$current .= urldecode($data_file[0]) . "';\n";
        		$i = 1;
        	}
        }
        $old_site_file = $app['twig']->render('old_site.txt');
        $current .= $old_site_file;
        fclose($handle);
    }
    if (isset($use_www)) {
        switch ($www) {
            case 1:
                $minus_www = $app['twig']->render('minus_www.txt');
                $current .= $minus_www;
                break;
            default:
                $plus_www = $app['twig']->render('plus_www.txt');
                $current .= $plus_www;
        }
    }
    if (isset($use_slash)) {
        switch ($slash) {
            case 0:
                $plus_slash = $app['twig']->render('plus_slash.txt');
                $current .= $plus_slash;
                break;
            default:
                $minus_slash = $app['twig']->render('minus_slash.txt');
                $current .= $minus_slash;
        }
    }
    if (isset($index)) {
        $index = $app['twig']->render('index.txt');
        $current .= $index;

    };
    file_put_contents($file_path, $current . "\n");
    $startY = 2018;
    $nowY = date('Y');
    if ($startY == $nowY)
        $years = $startY;
    else
        $years = $startY . " - " . $nowY;
    if ((isset($_COOKIE['lang'])) && $_COOKIE['lang'] == 'en')
        $content = array(
            'name' => 'Veronika',
            'title' => 'Take the file',
            'description' => "Please take the file with redirects here: ",
            'link' => $_SERVER['REQUEST_URI'] . '/uploads/' . $filename,
            'link_name' => 'Download',
            'language' => 'Change language:',
            'classActiveEn' => "class=active",
            'classActiveRu' => "",
            'return' => 'To page with generation',
            'years' => $years,
            'instruction' => 'Instruction',
            'use_slash' => 'Use / redirect',
            'use_www' => 'Use www redirect'
        );
    else
        $content = array(
            'name' => 'Veronika',
            'title' => 'Скачать',
            'description' => "Скачать файл с редиректами можно, перейдя по ссылке: ",
            'link' => $_SERVER['REQUEST_URI'] . '/uploads/' . $filename,
            'link_name' => 'Скачать',
            'language' => 'Изменить язык:',
            'classActiveRu' => "class=active",
            'classActiveEn' => "",
            'return' => 'На страницу генерации',
            'years' => $years,
            'instruction' => 'Инструкция',
            'use_slash' => 'Использовать / редирект',
            'use_www' => 'Использовать www редирект'
        );
    return $app['twig']->render('template-download.html', $content);
});
$app->run();




