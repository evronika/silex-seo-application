<?php
/**
 * Created by PhpStorm.
 * User: veronika
 * Date: 1.6.18
 * Time: 0.28
 */
namespace Silex;
use Symfony\Component\HttpFoundation\Request;

use Silex\Application;
use Silex\Route;
use Silex\Api\ControllerProviderInterface;
use Silex\ControllerCollection;

class Index implements ControllerProviderInterface {

    public function connect(Application $app) {
        $index = new ControllerCollection(new Route());

        $index->get('/', function() use ($app) {
            $page = $app['models']->load('Pages', 'index');

            return $page;
        });

        $index->post('/', function(Request $request) use ($app) {

            $index = $request->get('index');
            $www = $request->get('www');
            $slash = $request->get('slash');
            //work with csv
            $uploaddir = __DIR__ . '/uploads/';
            $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
            $file_path = $uploaddir . "redirects.php.txt";
            $file_out = $app['models']->load('Pages', 'createFile', $app, $index, $www, $slash, $uploadfile);
            file_put_contents($file_path, $file_out . "\n");
            return $app['twig']->render('template.html', array('name' => 'Veronika', 'title' => 'REDIRECTS', 'description' => "SELECT TYPES OF REDIRECTS"));
        });

        return $index;
    }
}