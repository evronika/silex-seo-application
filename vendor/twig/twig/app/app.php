<?php

require_once __DIR__.'../../../vendor/autoload.php';
$app = new Silex\Application();
$app['debug'] = true;
$loader = new Twig_Loader_Filesystem('../views/');
$twig = new Twig_Environment($loader);
echo $twig->render('template.html', array('name' => 'Hello world!'));