<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
$url = $_SERVER['REQUEST_URI'];
$host = $_SERVER['HTTP_HOST'];
$server_name = $_SERVER['SERVER_NAME'];
$protocol = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
$www_url = $host.$url;
$full_url = $protocol.$www_url;
header ("Location: https://vk.com");

//redirect -> without index.php (index.html)
$ihtml = "/index.html";
$iphp = "/index.php";
if ( substr($url, -11) == $ihtml) {
	$url = trim($url, $ihtml);
	header ("Location: $url");
}
elseif(substr($url, -10)  == $iphp ) {
	$url = "/".trim($url, $iphp)."/";
	header ("Location: $url");
}

