<?php
namespace Providers;

use Silex\Application;
use Silex\ControllerProviderInterface;

Class FileProvider implements ControllerProviderInterface{
    protected $index;
    protected $www;
    protected $slash;
    protected $file;



    public function __construct($index = 0, $www = 0, $slash = 0, $file='')
    {
        $this->index = $index;
        $this->www = $www;
        $this->slash = $slash;
        $this->file = $file;
    }

    public function createFile(Application $app){

//        $app = new Silex\Application();
//        $app['debug'] = true;
//        $app->register(new Silex\Provider\TwigServiceProvider(), array(
//            'twig.path' => __DIR__ . '/vendor/twig/twig/views/',
//        ));
        $old = $app['twig']->render('start.txt');
        $current = $old;
        if (($handle = fopen($uploadfile, "r")) !== FALSE && $uploadfile) {
            while (($data_file = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $current .= '$redirects[\'' . $data_file[0] . '\'] = \'' . $data_file[1] . "';\n";
            }
            $old_site_file = $app['twig']->render('old_site.txt');
            $current .= $old_site_file;
            fclose($handle);
        }
        if ($www == 1){
            $minus_www = $app['twig']->render('minus_www.txt');
            $current .= $minus_www;
        } else {
            $plus_www = $app['twig']->render('plus_www.txt');
            $current .= $plus_www;
        }
        if ($slash == 0) {
            $plus_slash = $app['twig']->render('plus_slash.txt');
            $current .= $plus_slash;
        } else {
            $minus_slash = $app['twig']->render('minus_slash.txt');
            $current .= $minus_slash;
        }
        if ($index == 1){
            $index = $app['twig']->render('index.txt');
            $current .= $index;

        }
        return $current;
    }
    
    public function readCsv($path){
        
    }
}
$app = new Silex\Application();
