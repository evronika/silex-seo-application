<?php
/**
 * Created by PhpStorm.
 * User: veronika
 * Date: 1.6.18
 * Time: 0.29
 */
namespace Silex\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;;
use Silex\Application;
use Silex\Api\BootableProviderInterface;

class ModelsServiceProvider implements ServiceProviderInterface, BootableProviderInterface {
    public function register(Container $app) {
        $app['models.path'] = array();
        $app['models']      = function($app) {
            return new Models($app);
        };
    }
    public function boot(Application $app) {

    }
}
class Models {
    private $app;
    public function __construct(Application $app) {
        $this->app = $app;
    }
    public function load($modelName, $modelMethod, $data = array()) {
        require_once $this->app['models.path'] . $modelName . '.php';

        $Model = new $modelName($this->app);

        return $Model->$modelMethod($data);
    }

}